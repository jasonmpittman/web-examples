<?php

session_start();

if (!isset($_SESSION['visits']))
{
    $_SESSION['visits'] = 1;
    $_SESSION["favcolor"] = "green";
    $_SESSION["favanimal"] = "cat";
}
else
{
    $_SESSION['visits']++;
    $_SESSION["favcolor"] = "red";
    $_SESSION["favanimal"] = "dog";
}

echo $_SESSION['visits'];
echo $_SESSION['favcolor'];
echo $_SESSION['favanimal'];

?>
