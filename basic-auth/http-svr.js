/*
  HTTP Basic Authentication Demo
  Jason M. Pittman
  Oct 2016
*/

var http = require('http');
var auth = require('basic-auth');

const PORT=8181;

function handleRequest(request, response){
  response.end('It works!');
}

var server = http.createServer(function (req, res){
  var credentials = auth(req);

  if (!credentials || credentials.name !== 'admin' || credentials.pass !== 'demo') {
    res.statusCode = 401;
    res.setHeader('WWW-Authenticate', 'Basic realm="basic-auth demo"');
    res.end('Access denied');
  } else {
    res.end('Access granted');
  }
})

server.listen(PORT);
